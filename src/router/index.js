import Vue from 'vue'
import VueRouter from 'vue-router'
import WordKeeper from "../components/WordKeeper";
import Favorite from "../components/Favorite";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'WordKeeper',
    component: WordKeeper
  },
  {
    path: '/favorite',
    name: 'Favorite',
    component: Favorite
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
